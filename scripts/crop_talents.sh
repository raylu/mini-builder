# Screenshot every talent and run this to get their effects dumped as
# text. Tesseract doesn't do a good job on the game font so fixing it up
# may actually be slower than transcribing the screenshots manually.
set -ux

GEOMETRY=472x544+1300+182  # correct for 1440x900 screenshots

mkdir -p cropped text
for file in *.png; do
  magick "$file" -crop "$GEOMETRY" cropped/"$file"
  tesseract -l eng cropped/"$file" text/"$file"
  mv text/"$file".txt "text/`head -n1 text/"$file".txt`.txt"
done
