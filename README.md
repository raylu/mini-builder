A build planner for [Mini Healer](https://store.steampowered.com/app/955740/Mini_Healer/)

## Setup Instructions

1. Install AssetRipper: https://assetripper.github.io/AssetRipper/articles/Downloads.html
1. Use AssetRipper on `Steam/steamapps/common/Mini Healer/MiniHealer_Data`
    * For the GUI version of AssetRipper, go to File -> Open Folder, select the `MiniHealer_Data` directory, and then go to Export -> Export all Files
    * For the console version, do something like (assuming Linux):
        ```
        mkdir -p scratch/ripped_assets  # this is .gitignore'd
        AssetRipperConsole ~/.steam/root/steamapps/common/Mini\ Healer/MiniHealer_Data -o scratch/ripped_assets
        ```
1. Install dotnet: https://dotnet.microsoft.com/download
1. Install npm: https://docs.npmjs.com/cli/v8/configuring-npm/install
1. Install JS dependencies: `npm install`
1. Prepare assets (depending on how you extracted the game assets your path to `ExportedProject` may be different):
    ```
    dotnet fsi scripts/prepareAssets.fsx scratch/ripped_assets/ExportedProject public/assets
    ```
1. Run the webpack dev server: `npm start`
