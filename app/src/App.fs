module App

open Elmish
open Elmish.React
open Fable.React
open Fable.React.Props
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

open Browser.CssExtensions

open type Msg.T

let assetsVersion = "0.92"

module UI =
  type TalentGrids =
    {
      Druid: TalentGrid.Model
      Priest: TalentGrid.Model
      Occultist: TalentGrid.Model
      Paladin: TalentGrid.Model
      CurrentTab: Msg.Class
    }
    member this.VisibleGrid() =
      match this.CurrentTab with
      | Msg.Druid -> this.Druid
      | Msg.Priest -> this.Priest
      | Msg.Occultist -> this.Occultist
      | Msg.Paladin -> this.Paladin

  type Model =
    {
      Party: Party.T
      Talents: array<Talent.T>
      TalentGrids: Option<TalentGrids>
      TalentRanks: int []
      TalentsSpent: int
    }
    member this.GetRank(talent: Talent.T) = this.TalentRanks.[talent.ExportIndex]
    member this.UpdateFragment() =
      Browser.Dom.window.location.replace $"#{Transport.export assetsVersion this.TalentRanks}"

  let init () : Model * Cmd<_> =
    {
      Party = Party.make 99 []
      Talents = [||]
      TalentGrids = None
      TalentRanks = [||]
      TalentsSpent = 0
    },
    Cmd.none

  let load _initial =
    let sub dispatch =
      promise {
        let! response = Fetch.fetch "assets/talents/talents.json" []
        let! text = response.text ()
        match
          Decode.Auto.fromString<array<Talent.T>>
            (
              text,
              extra = (Extra.empty
                       |> Extra.withCustom Attribute.T.encoder Attribute.T.decoder)
            )
          with
        | Ok (talents) -> dispatch (LoadTalents(talents))
        | Error (err) -> System.Console.WriteLine($"Error loading talents: {err}")
      }
      |> ignore
    Cmd.ofSub sub

  let update msg model =
    match msg with
    | LoadTalents (talents) ->
      let emptyTalentGrid clsIdx : TalentGrid.Model =
        {
          Talents =
            talents
            |> Array.filter (fun (tal: Talent.T) -> tal.Class = clsIdx)
          PointsSpent = 0
          Prereqs = [| 0; 4; 8; 12; 16; 20; 24; 28; 32; 36 |]
          Class = clsIdx
        }
      let fragment = Browser.Dom.window.location.hash.TrimStart('#')
      let talentRanks, talentsSpent = Transport.import fragment (talents.Length)
      let tgs =
        {
          Druid = emptyTalentGrid 0
          Priest = emptyTalentGrid 1
          Occultist = emptyTalentGrid 2
          Paladin = emptyTalentGrid 5
          CurrentTab = Msg.Druid
        }
      tgs.Druid.RecalculatePrereqs(talentRanks)
      tgs.Priest.RecalculatePrereqs(talentRanks)
      tgs.Occultist.RecalculatePrereqs(talentRanks)
      tgs.Paladin.RecalculatePrereqs(talentRanks)

      { model with
          Talents = talents
          TalentGrids = Some(tgs)
          TalentRanks = talentRanks
          TalentsSpent = talentsSpent
      },
      Cmd.ofMsg RefreshParty
    | UpdateTalentRanks (cls, idx, delta) ->
      let oldRank = model.TalentRanks.[idx]
      model.TalentRanks.[idx] <- oldRank + delta
      let tier = model.Talents.[idx].Tier
      match cls, model.TalentGrids with
      | 0, Some (tgs) -> tgs.Druid.AdjustPrereqs(tier, delta)
      | 1, Some (tgs) -> tgs.Priest.AdjustPrereqs(tier, delta)
      | 2, Some (tgs) -> tgs.Occultist.AdjustPrereqs(tier, delta)
      | 5, Some (tgs) -> tgs.Paladin.AdjustPrereqs(tier, delta)
      | _ -> ()
      { model with
          TalentsSpent = model.TalentsSpent + delta
      },
      Cmd.ofMsg RefreshParty
    | RefreshParty ->
      let talentAttributes =
        match model.TalentGrids with
        | None -> []
        | Some (tgs) ->
          tgs.Druid.Talents
          |> Seq.append tgs.Priest.Talents
          |> Seq.append tgs.Occultist.Talents
          |> Seq.append tgs.Paladin.Talents
          |> Seq.map (fun talent -> talent, model.GetRank(talent))
          |> Seq.filter (fun (_talent, rank) -> rank > 0)
          |> Seq.map (fun (talent, rank) -> talent.Name, talent.AsAttributes(rank))
          |> List.ofSeq

      let party = Party.make 99 talentAttributes
      let newModel = { model with Party = party }
      newModel.UpdateFragment()
      newModel, Cmd.none
    | SetTab (cls) ->
      match model.TalentGrids with
      | None -> model, Cmd.none
      | Some (tgs) ->
        { model with
            TalentGrids = Some({ tgs with CurrentTab = cls })
        },
        Cmd.none
    | ResetTalents ->
      match model.TalentGrids with
      | None -> model, Cmd.none
      | Some (tgs) ->
        tgs.Druid.Reset()
        tgs.Priest.Reset()
        tgs.Occultist.Reset()
        tgs.Paladin.Reset()
        model.TalentRanks
        |> Array.iteri (fun idx _ -> model.TalentRanks.[idx] <- 0)
        { model with TalentsSpent = 0 }, Cmd.ofMsg RefreshParty
    | ResetClass ->
      match model.TalentGrids with
      | None -> model, Cmd.none
      | Some (tgs) ->
        let tg = tgs.VisibleGrid()
        tg.Reset()
        let mutable talentDelta = 0
        for talent in tg.Talents do
          talentDelta <-
            talentDelta
            + model.TalentRanks.[talent.ExportIndex]
          model.TalentRanks.[talent.ExportIndex] <- 0

        { model with
            TalentsSpent = model.TalentsSpent - talentDelta
        },
        Cmd.ofMsg RefreshParty

  let view model dispatch =
    let tab cls pts =
      div [ Class "class-tab" ] [
        div [] [ str cls ]
        div [] [
          str (if pts > 0 then string pts else "\u00A0") // non-breaking space
        ]
      ]
    match model.TalentGrids with
    | Some (tgs) ->
      div [ Class "ui" ] [
        Tabber.render
          ([
            Msg.Druid, (tab "Druid" tgs.Druid.PointsSpent), tgs.Druid.Render(model.TalentRanks, dispatch)
            Msg.Priest, (tab "Priest" tgs.Priest.PointsSpent), tgs.Priest.Render(model.TalentRanks, dispatch)
            Msg.Occultist,
            (tab "Occultist" tgs.Occultist.PointsSpent),
            tgs.Occultist.Render(model.TalentRanks, dispatch)
            Msg.Paladin, (tab "Paladin" tgs.Paladin.PointsSpent), tgs.Paladin.Render(model.TalentRanks, dispatch)
          ])
          tgs.CurrentTab
          dispatch
        StatsPanel.render model.TalentsSpent model.Party dispatch
      ]
    | None ->
      div [ Class "ui" ] [
        let content = div [] [ str "Loading talents..." ]
        Tabber.render
          ([
            Msg.Druid, (tab "Druid" 0), content
            Msg.Priest, (tab "Priest" 0), content
            Msg.Occultist, (tab "Occultist" 0), content
            Msg.Paladin, (tab "Paladin" 0), content
          ])
          Msg.Druid
          dispatch
        StatsPanel.render model.TalentsSpent model.Party dispatch
      ]

Program.mkProgram UI.init UI.update UI.view
|> Program.withReactBatched "elmish-app"
|> Program.withSubscription UI.load
|> Program.run
