module Transport

open Fable.Core

type IJsonURL =
  abstract parse : msg:string -> obj
  abstract stringify : msg:obj -> string

[<Import("default", from="@jsonurl/jsonurl")>]
let JsonURL: IJsonURL = jsNative

let export (assetVersion: string) (talentRanks: array<int>) =
  let rec loop list idx =
    if idx >= Array.length talentRanks then
      list
    else
      let rank = talentRanks.[idx]
      if rank > 0 then
        loop (idx :: rank :: list) (idx + 1)
      else
        loop list (idx + 1)

  let data =
    {|
      V = assetVersion
      T = (loop [] 0 |> Array.ofList)
    |}
  JsonURL.stringify(data)

let import (export: string) (talentCount: int) =
  let talentRanks = Array.zeroCreate talentCount
  let mutable talentsSpent = 0
  try
    if export <> "" then
      let data: {| V: string; T: array<int> |} =
        try
          JsonURL.parse(export) :?> {| V: string; T: array<int> |}
        with
        | err -> raise (System.Exception($"Could not decode jsonurl: {err}"))

      match data.V with
      | "0.92" ->
        let rec loop idx =
          if idx >= Array.length data.T then
            ()
          else
            talentRanks.[data.T.[idx - 1]] <- data.T.[idx]
            talentsSpent <- talentsSpent + data.T.[idx]
            loop (idx + 2)
        loop 1
      | other -> raise (System.Exception($"Build data has unrecognized version: {other}"))
  with
  | err -> System.Console.Write($"Could not import build: {err}")

  talentRanks, talentsSpent
