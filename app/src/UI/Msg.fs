module Msg

open Fable.React

type Class =
  | Druid
  | Priest
  | Occultist
  | Paladin

type T =
  | LoadTalents of array<Talent.T>
  | RefreshParty
  | SetTab of Class
  | UpdateTalentRanks of int * int * int
  | ResetTalents
  | ResetClass
