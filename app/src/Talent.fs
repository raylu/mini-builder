module Talent

let TalentColumns = 5
let TalentTiers = 10

type Effect =
  {
    Stats: Attribute.StatStick
    Descriptions: List<string>
  }

type T =
  {
    Name: string
    PerRank: Effect
    Thresholds: List<int * Effect>
    Sacrifice: Effect
    MaxRanks: int

    Class: int
    Tier: int
    Column: int

    Icon: string
    ExportIndex: int
  }
  member this.AsAttributes(rank: int) =
    if rank = 0 then
      Map.empty
    else
      let allSticks: List<Attribute.StatStick> =
        this.Sacrifice.Stats
        :: (Attribute.stack this.PerRank.Stats rank)
           :: (this.Thresholds
               |> List.filter (fun (min: int, _) -> rank >= min)
               |> List.map (fun (_, effect) -> effect.Stats))

      Attribute.merge (allSticks)
