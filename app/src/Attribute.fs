module Attribute
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

type DisplayType =
  | Flat
  | Percent

type T =
  | Healpower
  | HealpowerIncr
  | HealingIncr
  | ShieldingIncr
  | CastSpeedIncr
  | CriticalHealChance
  | CriticalHealChancePerAura
  | CriticalHealMultiplier
  | CriticalHealMultiplierPerAura
  | MaximumMana
  | MaximumManaIncr
  | ManaRegeneration
  | ManaRegenerationIncr
  | ManaCostOfSkills
  | ManaCostOfChanneledSkills
  | AreaOfEffectIncr
  | AuraEffect
  | AuraManaReduction
  | AuraManaReductionIncr
  | MovementSpeedIncr

  | PartyAttackSpeedIncr
  | PartyCriticalStrikeChance
  | PartyCriticalStrikeDamage
  | PartyDamageBaseIncr
  | PartyDamageIncr
  | PartyElementalDamageBaseIncr
  | PartyElementalDamageIncr
  | PartyPhysicalDamageBase
  | PartyPhysicalDamageBaseIncr
  | PartyPhysicalDamageIncr
  | PartyFireDamageBase
  | PartyFireDamageBaseIncr
  | PartyFireDamageIncr
  | PartyIceDamageBase
  | PartyIceDamageBaseIncr
  | PartyIceDamageIncr
  | PartyLightningDamageBase
  | PartyLightningDamageBaseIncr
  | PartyLightningDamageIncr
  | PartyNemesisDamageBase
  | PartyNemesisDamageBaseIncr
  | PartyNemesisDamageIncr
  | PartyDamagePerAuraMore
  | PartyShieldedDamageMore

  | PartyMaximumHealth
  | PartyMaximumHealthIncr
  | PartyShieldedDamageTakenLess
  | PartyHealthRegeneration
  | PartyHealingReceivedIncr
  | PartyAllResist
  | PartyElementalResist
  | PartyPhysicalResist
  | PartyFireResist
  | PartyIceResist
  | PartyLightningResist
  | PartyNemesisResist
  | PartyDamageTakenIncr
  | PartyBlockChance
  | PartyDodgeChance
  | PartyMovementSpeedIncr

  | TankAttackSpeed
  | TankAttackSpeedIncr
  | TankCriticalStrikeChance
  | TankCriticalStrikeDamage
  | TankDamageBaseIncr
  | TankDamageIncr
  | TankElementalDamageBaseIncr
  | TankElementalDamageIncr
  | TankPhysicalDamageBase
  | TankPhysicalDamageBaseIncr
  | TankPhysicalDamageIncr
  | TankFireDamageBase
  | TankFireDamageBaseIncr
  | TankFireDamageIncr
  | TankIceDamageBase
  | TankIceDamageBaseIncr
  | TankIceDamageIncr
  | TankLightningDamageBase
  | TankLightningDamageBasePerLevel
  | TankLightningDamageBaseIncr
  | TankLightningDamageIncr
  | TankNemesisDamageBase
  | TankNemesisDamageBaseIncr
  | TankNemesisDamageIncr

  | TankMaximumHealth
  | TankMaximumHealthIncr
  | TankHealthRegeneration
  | TankElementalResist
  | TankPhysicalResist
  | TankFireResist
  | TankIceResist
  | TankLightningResist
  | TankNemesisResist
  | TankBlockChance
  | TankDodgeChance
  | TankMovementSpeedIncr

  | BerzerkerAttackSpeed
  | BerzerkerAttackSpeedIncr
  | BerzerkerCriticalStrikeChance
  | BerzerkerCriticalStrikeDamage
  | BerzerkerDamageBaseIncr
  | BerzerkerDamageIncr
  | BerzerkerElementalDamageBaseIncr
  | BerzerkerElementalDamageIncr
  | BerzerkerPhysicalDamageBase
  | BerzerkerPhysicalDamageBaseIncr
  | BerzerkerPhysicalDamageIncr
  | BerzerkerFireDamageBase
  | BerzerkerFireDamageBaseIncr
  | BerzerkerFireDamageIncr
  | BerzerkerIceDamageBase
  | BerzerkerIceDamageBaseIncr
  | BerzerkerIceDamageIncr
  | BerzerkerLightningDamageBase
  | BerzerkerLightningDamageBasePerLevel
  | BerzerkerLightningDamageBaseIncr
  | BerzerkerLightningDamageIncr
  | BerzerkerNemesisDamageBase
  | BerzerkerNemesisDamageBaseIncr
  | BerzerkerNemesisDamageIncr

  | BerzerkerMaximumHealth
  | BerzerkerMaximumHealthIncr
  | BerzerkerHealthRegeneration
  | BerzerkerElementalResist
  | BerzerkerPhysicalResist
  | BerzerkerFireResist
  | BerzerkerIceResist
  | BerzerkerLightningResist
  | BerzerkerNemesisResist
  | BerzerkerBlockChance
  | BerzerkerDodgeChance
  | BerzerkerMovementSpeedIncr

  | HealerAttackSpeed
  | HealerAttackSpeedIncr
  | HealerCriticalStrikeChance
  | HealerCriticalStrikeDamage
  | HealerDamageBaseIncr
  | HealerDamageIncr
  | HealerElementalDamageBaseIncr
  | HealerElementalDamageIncr
  | HealerPhysicalDamageBase
  | HealerPhysicalDamageBaseIncr
  | HealerPhysicalDamageIncr
  | HealerFireDamageBase
  | HealerFireDamageBaseIncr
  | HealerFireDamageIncr
  | HealerIceDamageBase
  | HealerIceDamageBaseIncr
  | HealerIceDamageIncr
  | HealerLightningDamageBase
  | HealerLightningDamageBasePerLevel
  | HealerLightningDamageBaseIncr
  | HealerLightningDamageIncr
  | HealerNemesisDamageBase
  | HealerNemesisDamageBaseIncr
  | HealerNemesisDamageIncr

  | HealerMaximumHealth
  | HealerMaximumHealthIncr
  | HealerHealthRegeneration
  | HealerElementalResist
  | HealerPhysicalResist
  | HealerFireResist
  | HealerIceResist
  | HealerLightningResist
  | HealerNemesisResist
  | HealerBlockChance
  | HealerDodgeChance
  | HealerMovementSpeedIncr

  | RangerAttackSpeed
  | RangerAttackSpeedIncr
  | RangerCriticalStrikeChance
  | RangerCriticalStrikeDamage
  | RangerDamageBaseIncr
  | RangerDamageIncr
  | RangerElementalDamageBaseIncr
  | RangerElementalDamageIncr
  | RangerPhysicalDamageBase
  | RangerPhysicalDamageBaseIncr
  | RangerPhysicalDamageIncr
  | RangerFireDamageBase
  | RangerFireDamageBaseIncr
  | RangerFireDamageIncr
  | RangerIceDamageBase
  | RangerIceDamageBaseIncr
  | RangerIceDamageIncr
  | RangerLightningDamageBase
  | RangerLightningDamageBasePerLevel
  | RangerLightningDamageBaseIncr
  | RangerLightningDamageIncr
  | RangerNemesisDamageBase
  | RangerNemesisDamageBaseIncr
  | RangerNemesisDamageIncr

  | RangerMaximumHealth
  | RangerMaximumHealthIncr
  | RangerHealthRegeneration
  | RangerElementalResist
  | RangerPhysicalResist
  | RangerFireResist
  | RangerIceResist
  | RangerLightningResist
  | RangerNemesisResist
  | RangerBlockChance
  | RangerDodgeChance
  | RangerMovementSpeedIncr

  | EnemyBlockChance
  | EnemyDodgeChance

  // Conversions
  | ConvHealpowerPartyHealthRegeneration

  // Skill-specific
  | SingleTargetNonHotHealingSkillEffect
  | BuffSkillEffect
  | AmpleHealEffect
  | AncientRootsGrant
  | AscensionGrant
  | AngelicInfusionBlessedWingsCooldownLess
  | AngelicInfusionBlessedWingsManaCostIncr
  | AngelicInfusionDuration
  | BanishAttackSpeedIncr
  | BanishCooldown
  | BanishElementalResistPenalty
  | BlessingOfGaiaDuration
  | BlessingOfGaiaEffect
  | BlossomAllResist
  | BlossomEnabled
  | BlossomHealpowerRatio
  | CataclysmDamageRatio
  | CataclysmEnabled
  | ChaosStrikeCooldownOnBerzerkerCriticalStrike
  | ChaosStrikeDamageIncr
  | ChaosStrikeDamageRatio
  | ChaosStrikeEnabled
  | CleansingRayAttackSpeedIncr
  | CleansingRayBaseDamageIncr
  | CleansingRaySelfDamage
  | CleansingRayJudgementRatio
  | CleansingRayManaCostIncr
  | CleansingRayZealBaseDamageIncr
  | CleansingRayZealAttackSpeedIncr
  | CleansingRayZealEnabled
  | CleansingRayZealHealingReceivedReduc
  | DispelCooldownIncr
  | DivineFocusBeginsCooldownImmediately
  | DivineFocusCooldown
  | DivineMendEffect
  | DivineMendRedemptionEffect
  | DivineMendRedemptionMaxStacks
  | FlashHealCooldown
  | FlashHealEffect
  | FlashHealInfusedLuminosityChance
  | FlashHealInfusedLuminosityHealingMore
  | FlashHealInfusedLuminosityDamageRatio
  | FlashHealInfusedLuminosityAllResistPenalty
  | FlashHealManaCostIncr
  | HammerOfReckoningCanCrit
  | HammerOfReckoningCharges
  | HammerOfReckoningEffect
  | HolyShieldCooldown
  | HolyShieldDuration
  | HolyShieldEffect
  | InspireEnabled
  | InspirePartyDamageIncr
  | InspireRegeneration
  | InspireRegenerationRatio
  | HeavensMelodyCooldown
  | HeavensMelodyCooldownOnRadiantHeal
  | IronVinesDuration
  | IronVinesHealpowerIncr
  | LesserHealEffect
  | LesserHealExtraTargetRatio
  | MindRushAttackSpeedIncr
  | MindRushCastTime
  | MindRushDuration
  | MindRushNemesisResist
  | NaturesGiftCanCrit
  | NaturesGiftCooldownIncr
  | NaturesGiftEffect
  | RadiantHealCastTime
  | RadiantHealCooldown
  | RadiantHealEffect
  | RadiantHealShieldRatio
  | RadiantHealTrailOfFateRatio
  | RapidShotCastTime
  | RapidShotCooldown
  | RapidShotEnabled
  | RapidShotDamageIncr
  | RapidShotDamageRatio
  | RapidShotArrows
  | RaptureRatio
  | ReinforcedBarrierChance
  | ReinforcedBarrierMaximumHealthRatio
  | RejuvenateCooldownIncr
  | RejuvenateCooldownOnHealerCrit
  | RejuvenateCooldownOnSpiritRitualTick
  | RejuvenateHealRatio
  | RejuvenateManaPerTick
  | RejuvenatePartyIceDamageBase
  | RejuvenatePartyIceDamageBaseIncr
  | RenewCanCrit
  | RenewCooldown
  | RenewCooldownIncr
  | RenewDuration
  | RenewEffect
  | RenewFocusedGloryHealingMore
  | RenewManaRestoreChance
  | RestorationTotemFireDamageBaseIncr
  | RestorationTotemIceDamageBaseIncr
  | RestorationTotemLightningDamageBaseIncr
  | RestorationTotemCharges
  | RestorationTotemCooldownIncr
  | RestorationTotemDamageTakenReduc
  | RestorationTotemEffect
  | RestorationTotemHealingReceivedIncr
  | ReviveGrant
  | SacredInfusionElementalResist
  | SacredInfusionHealthRegeneration
  | SacredInfusionPhysicalResist
  | SalvationEnabled
  | SalvationDamageTakenReduc
  | SalvationFireLightningDamageIncr
  | SanctuaryDuration
  | SanctuaryCooldown
  | SealOfLightCanCritHeal
  | SealOfLightCanCrit
  | SealOfLightEffect
  | SealOfLightMaxStacks
  | ShadowRippleEffect
  | ShadowRippleCriticalHealChance
  | ShadowRippleOverhealRatio
  | SiphonLifeDamageIncr
  | SiphonLifeDuration
  | SiphonLifeEffect
  | SiphonLifeExtraHealingTargets
  | SiphonLifeHealingIncr
  | SmiteCooldown
  | SmiteCooldownOnHealerCrit
  | SmiteDamageIncr
  | SmiteTwistOfFateDamageIncr
  | SpiritOrbEffect
  | SpiritOrbIceDamageHealRatio
  | SpiritOrbIceResistDebuff
  | SpiritRitualCanCrit
  | SpiritRitualEffect
  | SpiritRitualHealingReceivedIncr
  | SpiritRitualRefreshChance
  | StarCallDamageIncr
  | StarCallSplashChance
  | StarCallSplashDamageRatio
  | ToughenDamageTakenIncr
  | ToughenDuration
  | ToughenEffect
  | VitalizeGrant
  | VoidZoneCharges
  | VoidZoneShadowEntropyDamageRatio
  | VoidZoneShadowEntropyEnabled

  // Unique talent effects
  | AstralShift
  | BlessedCompanyTankPhysicalResist
  | BlessedCompanyTankDuration
  | BlessedCompanyBerzerkerRangerDamageRatio
  | BlessedCompanyHealerHealRatio
  | BlessingOfTheForestHealingRatio
  | ConvictionChance
  | ConvictionDamageRatio
  | ConvictionDamageGrantReduc
  | ConvocationChannelRejuvenate
  | DecayAuraDamageRatio
  | DecayAuraDamageTakenIncr
  | DeceptionPartyAllResistsBonus
  | DeceptionPartyAllResistsPenalty
  | DeepMindRefundChance
  | DevotionBlockChance
  | DevotionDamageIncr
  | DevotionEnabled
  | DevotionPhysicalNemesisResist
  | DivineAegisHealRatio
  | DivineDecreeBlockDodgeTimer
  | DivineDecreeHealingTimer
  | DivinePurposeHealpowerIncr
  | EbbAndFlowHealingReceivedLess
  | EbbAndFlowHealingReceivedMore
  | EbbAndFlowResistance
  | EbbAndFlowResistancePenalty
  | EradicationDamageTakenMore
  | FeatheredFootworkManaCostIncr
  | FeatheredFootworkMoveSpeedReduc
  | FortunateFaithChance
  | GlimmerOfHopeEnabled
  | HeavensAidShieldRatio
  | HeightenedSensesEnabled
  | HeightenedSensesHealpower
  | HeightenedSensesIceDamageBase
  | HeroismRedirectRatio
  | ImmolateRatio
  | ImmovableFortressSlowEffectReduce
  | ImmovableFortressSilenceDurationReduce
  | ImmovableFortressDamageTakenIncr
  | InquisitionCooldownMod
  | IntoTheFrayEnabled
  | LastStandDamageTakenLess
  | LastStandDamageMore
  | LeaveNoOneBehindEnabled
  | LightsGuidanceDamageTakenReduc
  | MarkOfDeathChance
  | MarkOfDeathDamageRatio
  | MarkOfDeathDamageTakenIncrRatio
  | NemesisInfusionChance
  | PathsEndHealpowerIncr
  | PoisonChance
  | PoisonCriticalStrikeChanceRatio
  | PoisonDamage
  | PoisonDamageIncr
  | PoisonDamageRatio
  | PoisonMaxStacks
  | PureInHeartRatio
  | ReactiveInstinctDodgeChance
  | RingOfInfluenceHealingReceivedIncr
  | ShadeRegenerationRatio
  | ShieldBashDamageRatio
  | ShieldBashEnabled
  | SoulBoostPhysicalDamageIncr
  | SoulBoostFireDamageIncr
  | SurvivalInstinctDodgeChance
  | SurvivalInstinctEnabled
  | SurvivalInstinctHealingReceived
  | SwordAndBoardPaladinSkillLevels
  | TauntDamageLess
  | TauntDamageTakenMore
  | TauntEnabled
  | ThunderAssaultDamage
  | ThunderAssaultDamageRatio
  | ThunderAssaultEnabled
  | TimeInNeedHealingRatio
  | TyphoonAttackSpeedSlow
  | TyphoonEnabled
  | TyphoonPhysicalIceDamageTakenIncr
  | VampirismBerzerker
  | VampirismHealer
  | VampirismRanger
  | VampirismRatio
  | VampirismTank
  | VoidChainsHealingReceivedIncr
  | VoidInfluenceCastTimeIncr
  | VoodooMagicRatio
  | WickedBloomRenewChance
  | WickedBloomShadowRippleChance
  | WorshipShieldRatio

  // Sacrifice
  | Sacrifice
  | BanishSacrificeAttackSpeedRatioIncr
  | CataclysmSacrificeDamageMore
  | CleansingRaySacrificeBaseDamageIncr
  | DecayAuraSacrificeRatio
  | EradicationDamageSacrificeRatio
  | NemesisInfusionSacrificeRatio
  | SacrificeBerzerkerAttackSpeedIncr
  | SacrificeHealpowerIncr
  | SacrificeMaximumMana
  | SacrificePartyBlockChance
  | SacrificePartyDodgeChance
  | SacrificePartyMaximumHealthIncr
  | SiphonLifeSacrificeDamageIncr
  | ShadowRippleSacrificeOverhealRatio
  | VampirismSacrificeRatio
  | VoidChainsSacrificeHealingReceivedIncr
  | VoidZoneShadowSacrificeEntropyDamageRatio
  | WickedBloomSacrificeChance

  member this.DisplayType =
    match this with
    | Healpower -> Flat
    | HealpowerIncr -> Percent
    | HealingIncr -> Percent
    | ShieldingIncr -> Percent
    | CastSpeedIncr -> Percent
    | CriticalHealChance -> Percent
    | CriticalHealChancePerAura -> Percent
    | CriticalHealMultiplier -> Percent
    | CriticalHealMultiplierPerAura -> Percent
    | MaximumMana -> Flat
    | MaximumManaIncr -> Percent
    | ManaRegeneration -> Flat
    | ManaRegenerationIncr -> Percent
    | ManaCostOfSkills -> Flat
    | ManaCostOfChanneledSkills -> Flat
    | AreaOfEffectIncr -> Percent
    | AuraEffect -> Percent
    | AuraManaReduction -> Percent
    | AuraManaReductionIncr -> Percent
    | MovementSpeedIncr -> Percent

    | PartyAttackSpeedIncr -> Percent
    | PartyCriticalStrikeChance -> Percent
    | PartyCriticalStrikeDamage -> Percent
    | PartyDamageIncr -> Percent
    | PartyDamageBaseIncr -> Percent
    | PartyElementalDamageIncr -> Percent
    | PartyElementalDamageBaseIncr -> Percent
    | PartyPhysicalDamageBase -> Flat
    | PartyPhysicalDamageBaseIncr -> Percent
    | PartyPhysicalDamageIncr -> Percent
    | PartyFireDamageBase -> Flat
    | PartyFireDamageBaseIncr -> Percent
    | PartyFireDamageIncr -> Percent
    | PartyIceDamageBase -> Flat
    | PartyIceDamageBaseIncr -> Percent
    | PartyIceDamageIncr -> Percent
    | PartyLightningDamageBase -> Flat
    | PartyLightningDamageBaseIncr -> Percent
    | PartyLightningDamageIncr -> Percent
    | PartyNemesisDamageBase -> Flat
    | PartyNemesisDamageBaseIncr -> Percent
    | PartyNemesisDamageIncr -> Percent
    | PartyDamagePerAuraMore -> Percent
    | PartyShieldedDamageMore -> Percent

    | PartyMaximumHealth -> Flat
    | PartyMaximumHealthIncr -> Percent
    | PartyShieldedDamageTakenLess -> Percent
    | PartyHealthRegeneration -> Flat
    | PartyHealingReceivedIncr -> Percent
    | PartyAllResist -> Percent
    | PartyElementalResist -> Percent
    | PartyPhysicalResist -> Percent
    | PartyFireResist -> Percent
    | PartyIceResist -> Percent
    | PartyLightningResist -> Percent
    | PartyNemesisResist -> Percent
    | PartyDamageTakenIncr -> Percent
    | PartyBlockChance -> Percent
    | PartyDodgeChance -> Percent
    | PartyMovementSpeedIncr -> Percent

    | TankAttackSpeed -> Flat
    | TankAttackSpeedIncr -> Percent
    | TankCriticalStrikeChance -> Percent
    | TankCriticalStrikeDamage -> Percent
    | TankDamageIncr -> Percent
    | TankDamageBaseIncr -> Percent
    | TankElementalDamageIncr -> Percent
    | TankElementalDamageBaseIncr -> Percent
    | TankPhysicalDamageBase -> Flat
    | TankPhysicalDamageBaseIncr -> Percent
    | TankPhysicalDamageIncr -> Percent
    | TankFireDamageBase -> Flat
    | TankFireDamageBaseIncr -> Percent
    | TankFireDamageIncr -> Percent
    | TankIceDamageBase -> Flat
    | TankIceDamageBaseIncr -> Percent
    | TankIceDamageIncr -> Percent
    | TankLightningDamageBase -> Flat
    | TankLightningDamageBasePerLevel -> Flat
    | TankLightningDamageBaseIncr -> Percent
    | TankLightningDamageIncr -> Percent
    | TankNemesisDamageBase -> Flat
    | TankNemesisDamageBaseIncr -> Percent
    | TankNemesisDamageIncr -> Percent

    | TankMaximumHealth -> Flat
    | TankMaximumHealthIncr -> Percent
    | TankHealthRegeneration -> Flat
    | TankElementalResist -> Percent
    | TankPhysicalResist -> Percent
    | TankFireResist -> Percent
    | TankIceResist -> Percent
    | TankLightningResist -> Percent
    | TankNemesisResist -> Percent
    | TankBlockChance -> Percent
    | TankDodgeChance -> Percent
    | TankMovementSpeedIncr -> Percent

    | BerzerkerAttackSpeed -> Flat
    | BerzerkerAttackSpeedIncr -> Percent
    | BerzerkerCriticalStrikeChance -> Percent
    | BerzerkerCriticalStrikeDamage -> Percent
    | BerzerkerDamageIncr -> Percent
    | BerzerkerDamageBaseIncr -> Percent
    | BerzerkerElementalDamageIncr -> Percent
    | BerzerkerElementalDamageBaseIncr -> Percent
    | BerzerkerPhysicalDamageBase -> Flat
    | BerzerkerPhysicalDamageBaseIncr -> Percent
    | BerzerkerPhysicalDamageIncr -> Percent
    | BerzerkerFireDamageBase -> Flat
    | BerzerkerFireDamageBaseIncr -> Percent
    | BerzerkerFireDamageIncr -> Percent
    | BerzerkerIceDamageBase -> Flat
    | BerzerkerIceDamageBaseIncr -> Percent
    | BerzerkerIceDamageIncr -> Percent
    | BerzerkerLightningDamageBase -> Flat
    | BerzerkerLightningDamageBasePerLevel -> Flat
    | BerzerkerLightningDamageBaseIncr -> Percent
    | BerzerkerLightningDamageIncr -> Percent
    | BerzerkerNemesisDamageBase -> Flat
    | BerzerkerNemesisDamageBaseIncr -> Percent
    | BerzerkerNemesisDamageIncr -> Percent

    | BerzerkerMaximumHealth -> Flat
    | BerzerkerMaximumHealthIncr -> Percent
    | BerzerkerHealthRegeneration -> Flat
    | BerzerkerElementalResist -> Percent
    | BerzerkerPhysicalResist -> Percent
    | BerzerkerFireResist -> Percent
    | BerzerkerIceResist -> Percent
    | BerzerkerLightningResist -> Percent
    | BerzerkerNemesisResist -> Percent
    | BerzerkerBlockChance -> Percent
    | BerzerkerDodgeChance -> Percent
    | BerzerkerMovementSpeedIncr -> Percent

    | HealerAttackSpeed -> Flat
    | HealerAttackSpeedIncr -> Percent
    | HealerCriticalStrikeChance -> Percent
    | HealerCriticalStrikeDamage -> Percent
    | HealerDamageIncr -> Percent
    | HealerDamageBaseIncr -> Percent
    | HealerElementalDamageIncr -> Percent
    | HealerElementalDamageBaseIncr -> Percent
    | HealerPhysicalDamageBase -> Flat
    | HealerPhysicalDamageBaseIncr -> Percent
    | HealerPhysicalDamageIncr -> Percent
    | HealerFireDamageBase -> Flat
    | HealerFireDamageBaseIncr -> Percent
    | HealerFireDamageIncr -> Percent
    | HealerIceDamageBase -> Flat
    | HealerIceDamageBaseIncr -> Percent
    | HealerIceDamageIncr -> Percent
    | HealerLightningDamageBase -> Flat
    | HealerLightningDamageBasePerLevel -> Flat
    | HealerLightningDamageBaseIncr -> Percent
    | HealerLightningDamageIncr -> Percent
    | HealerNemesisDamageBase -> Flat
    | HealerNemesisDamageBaseIncr -> Percent
    | HealerNemesisDamageIncr -> Percent

    | HealerMaximumHealth -> Flat
    | HealerMaximumHealthIncr -> Percent
    | HealerHealthRegeneration -> Flat
    | HealerElementalResist -> Percent
    | HealerPhysicalResist -> Percent
    | HealerFireResist -> Percent
    | HealerIceResist -> Percent
    | HealerLightningResist -> Percent
    | HealerNemesisResist -> Percent
    | HealerBlockChance -> Percent
    | HealerDodgeChance -> Percent
    | HealerMovementSpeedIncr -> Percent

    | RangerAttackSpeed -> Flat
    | RangerAttackSpeedIncr -> Percent
    | RangerCriticalStrikeChance -> Percent
    | RangerCriticalStrikeDamage -> Percent
    | RangerDamageIncr -> Percent
    | RangerDamageBaseIncr -> Percent
    | RangerElementalDamageIncr -> Percent
    | RangerElementalDamageBaseIncr -> Percent
    | RangerPhysicalDamageBase -> Flat
    | RangerPhysicalDamageBaseIncr -> Percent
    | RangerPhysicalDamageIncr -> Percent
    | RangerFireDamageBase -> Flat
    | RangerFireDamageBaseIncr -> Percent
    | RangerFireDamageIncr -> Percent
    | RangerIceDamageBase -> Flat
    | RangerIceDamageBaseIncr -> Percent
    | RangerIceDamageIncr -> Percent
    | RangerLightningDamageBase -> Flat
    | RangerLightningDamageBasePerLevel -> Flat
    | RangerLightningDamageBaseIncr -> Percent
    | RangerLightningDamageIncr -> Percent
    | RangerNemesisDamageBase -> Flat
    | RangerNemesisDamageBaseIncr -> Percent
    | RangerNemesisDamageIncr -> Percent

    | RangerMaximumHealth -> Flat
    | RangerMaximumHealthIncr -> Percent
    | RangerHealthRegeneration -> Flat
    | RangerElementalResist -> Percent
    | RangerPhysicalResist -> Percent
    | RangerFireResist -> Percent
    | RangerIceResist -> Percent
    | RangerLightningResist -> Percent
    | RangerNemesisResist -> Percent
    | RangerBlockChance -> Percent
    | RangerDodgeChance -> Percent
    | RangerMovementSpeedIncr -> Percent

    | EnemyBlockChance -> Percent
    | EnemyDodgeChance -> Percent

    // Conversions
    | ConvHealpowerPartyHealthRegeneration -> Percent

    // Skill-specific
    | SingleTargetNonHotHealingSkillEffect -> Percent
    | BuffSkillEffect -> Percent
    | AmpleHealEffect -> Percent
    | AncientRootsGrant -> Flat
    | AscensionGrant -> Flat
    | AngelicInfusionBlessedWingsCooldownLess -> Percent
    | AngelicInfusionBlessedWingsManaCostIncr -> Percent
    | AngelicInfusionDuration -> Flat
    | BanishAttackSpeedIncr -> Percent
    | BanishCooldown -> Flat
    | BanishElementalResistPenalty -> Percent
    | BlessingOfGaiaDuration -> Flat
    | BlessingOfGaiaEffect -> Percent
    | BlossomAllResist -> Percent
    | BlossomEnabled -> Flat
    | BlossomHealpowerRatio -> Percent
    | CataclysmDamageRatio -> Percent
    | CataclysmEnabled -> Flat
    | ChaosStrikeCooldownOnBerzerkerCriticalStrike -> Flat
    | ChaosStrikeDamageIncr -> Percent
    | ChaosStrikeDamageRatio -> Percent
    | ChaosStrikeEnabled -> Flat
    | CleansingRayAttackSpeedIncr -> Percent
    | CleansingRayBaseDamageIncr -> Percent
    | CleansingRaySelfDamage -> Percent
    | CleansingRayJudgementRatio -> Percent
    | CleansingRayManaCostIncr -> Percent
    | CleansingRayZealBaseDamageIncr -> Percent
    | CleansingRayZealAttackSpeedIncr -> Percent
    | CleansingRayZealEnabled -> Percent
    | CleansingRayZealHealingReceivedReduc -> Percent
    | DispelCooldownIncr -> Percent
    | DivineFocusBeginsCooldownImmediately -> Flat
    | DivineFocusCooldown -> Flat
    | DivineMendEffect -> Percent
    | DivineMendRedemptionEffect -> Percent
    | DivineMendRedemptionMaxStacks -> Flat
    | FlashHealCooldown -> Flat
    | FlashHealEffect -> Percent
    | FlashHealInfusedLuminosityChance -> Percent
    | FlashHealInfusedLuminosityHealingMore -> Percent
    | FlashHealInfusedLuminosityDamageRatio -> Percent
    | FlashHealInfusedLuminosityAllResistPenalty -> Percent
    | FlashHealManaCostIncr -> Percent
    | HammerOfReckoningCanCrit -> Flat
    | HammerOfReckoningCharges -> Flat
    | HammerOfReckoningEffect -> Percent
    | HeavensMelodyCooldown -> Flat
    | HeavensMelodyCooldownOnRadiantHeal -> Flat
    | HolyShieldCooldown -> Flat
    | HolyShieldDuration -> Flat
    | HolyShieldEffect -> Percent
    | InspireEnabled -> Flat
    | InspirePartyDamageIncr -> Percent
    | InspireRegeneration -> Flat
    | InspireRegenerationRatio -> Percent
    | IronVinesDuration -> Flat
    | IronVinesHealpowerIncr -> Percent
    | LesserHealEffect -> Percent
    | LesserHealExtraTargetRatio -> Percent
    | MindRushAttackSpeedIncr -> Percent
    | MindRushCastTime -> Flat
    | MindRushDuration -> Flat
    | MindRushNemesisResist -> Percent
    | NaturesGiftCanCrit -> Flat
    | NaturesGiftCooldownIncr -> Percent
    | NaturesGiftEffect -> Percent
    | RadiantHealCastTime -> Flat
    | RadiantHealCooldown -> Flat
    | RadiantHealEffect -> Percent
    | RadiantHealShieldRatio -> Percent
    | RadiantHealTrailOfFateRatio -> Percent
    | RapidShotCastTime -> Flat
    | RapidShotCooldown -> Flat
    | RapidShotEnabled -> Flat
    | RapidShotDamageIncr -> Percent
    | RapidShotDamageRatio -> Percent
    | RapidShotArrows -> Flat
    | RaptureRatio -> Percent
    | ReinforcedBarrierChance -> Percent
    | ReinforcedBarrierMaximumHealthRatio -> Percent
    | RejuvenateCooldownIncr -> Percent
    | RejuvenateCooldownOnHealerCrit -> Flat
    | RejuvenateCooldownOnSpiritRitualTick -> Flat
    | RejuvenateHealRatio -> Percent
    | RejuvenateManaPerTick -> Flat
    | RejuvenatePartyIceDamageBase -> Flat
    | RejuvenatePartyIceDamageBaseIncr -> Percent
    | RenewCanCrit -> Flat
    | RenewCooldown -> Flat
    | RenewCooldownIncr -> Percent
    | RenewDuration -> Flat
    | RenewEffect -> Percent
    | RenewFocusedGloryHealingMore -> Percent
    | RenewManaRestoreChance -> Percent
    | RestorationTotemFireDamageBaseIncr -> Percent
    | RestorationTotemIceDamageBaseIncr -> Percent
    | RestorationTotemLightningDamageBaseIncr -> Percent
    | RestorationTotemCharges -> Flat
    | RestorationTotemCooldownIncr -> Percent
    | RestorationTotemDamageTakenReduc -> Percent
    | RestorationTotemEffect -> Percent
    | RestorationTotemHealingReceivedIncr -> Percent
    | ReviveGrant -> Flat
    | SacredInfusionElementalResist -> Percent
    | SacredInfusionHealthRegeneration -> Flat
    | SacredInfusionPhysicalResist -> Percent
    | SalvationEnabled -> Flat
    | SalvationDamageTakenReduc -> Percent
    | SalvationFireLightningDamageIncr -> Percent
    | SanctuaryDuration -> Flat
    | SanctuaryCooldown -> Flat
    | SealOfLightCanCritHeal -> Flat
    | SealOfLightCanCrit -> Flat
    | SealOfLightEffect -> Percent
    | SealOfLightMaxStacks -> Flat
    | ShadowRippleEffect -> Percent
    | ShadowRippleCriticalHealChance -> Percent
    | ShadowRippleOverhealRatio -> Percent
    | SiphonLifeDamageIncr -> Percent
    | SiphonLifeDuration -> Flat
    | SiphonLifeEffect -> Percent
    | SiphonLifeExtraHealingTargets -> Flat
    | SiphonLifeHealingIncr -> Percent
    | SmiteCooldown -> Flat
    | SmiteCooldownOnHealerCrit -> Flat
    | SmiteDamageIncr -> Percent
    | SmiteTwistOfFateDamageIncr -> Percent
    | SpiritOrbEffect -> Percent
    | SpiritOrbIceDamageHealRatio -> Percent
    | SpiritOrbIceResistDebuff -> Percent
    | SpiritRitualCanCrit -> Flat
    | SpiritRitualEffect -> Percent
    | SpiritRitualHealingReceivedIncr -> Percent
    | SpiritRitualRefreshChance -> Percent
    | StarCallDamageIncr -> Percent
    | StarCallSplashChance -> Percent
    | StarCallSplashDamageRatio -> Percent
    | ToughenDamageTakenIncr -> Percent
    | ToughenDuration -> Flat
    | ToughenEffect -> Percent
    | VitalizeGrant -> Flat
    | VoidZoneCharges -> Flat
    | VoidZoneShadowEntropyDamageRatio -> Percent
    | VoidZoneShadowEntropyEnabled -> Flat

    // Unique talent effects
    | AstralShift -> Percent
    | BlessedCompanyTankPhysicalResist -> Percent
    | BlessedCompanyTankDuration -> Flat
    | BlessedCompanyBerzerkerRangerDamageRatio -> Percent
    | BlessedCompanyHealerHealRatio -> Percent
    | BlessingOfTheForestHealingRatio -> Percent
    | ConvictionChance -> Percent
    | ConvictionDamageRatio -> Percent
    | ConvictionDamageGrantReduc -> Percent
    | ConvocationChannelRejuvenate -> Flat
    | DecayAuraDamageRatio -> Percent
    | DecayAuraDamageTakenIncr -> Percent
    | DeceptionPartyAllResistsBonus -> Percent
    | DeceptionPartyAllResistsPenalty -> Percent
    | DeepMindRefundChance -> Percent
    | DevotionBlockChance -> Percent
    | DevotionDamageIncr -> Percent
    | DevotionEnabled -> Flat
    | DevotionPhysicalNemesisResist -> Percent
    | DivineAegisHealRatio -> Percent
    | DivineDecreeBlockDodgeTimer -> Flat
    | DivineDecreeHealingTimer -> Flat
    | DivinePurposeHealpowerIncr -> Percent
    | EbbAndFlowHealingReceivedLess -> Percent
    | EbbAndFlowHealingReceivedMore -> Percent
    | EbbAndFlowResistance -> Percent
    | EbbAndFlowResistancePenalty -> Percent
    | EradicationDamageTakenMore -> Percent
    | FeatheredFootworkManaCostIncr -> Percent
    | FeatheredFootworkMoveSpeedReduc -> Percent
    | FortunateFaithChance -> Percent
    | GlimmerOfHopeEnabled -> Flat
    | HeavensAidShieldRatio -> Percent
    | HeightenedSensesEnabled -> Flat
    | HeightenedSensesHealpower -> Flat
    | HeightenedSensesIceDamageBase -> Flat
    | HeroismRedirectRatio -> Percent
    | ImmolateRatio -> Percent
    | ImmovableFortressSlowEffectReduce -> Percent
    | ImmovableFortressSilenceDurationReduce -> Percent
    | ImmovableFortressDamageTakenIncr -> Percent
    | InquisitionCooldownMod -> Flat
    | IntoTheFrayEnabled -> Flat
    | LastStandDamageTakenLess -> Percent
    | LastStandDamageMore -> Percent
    | LeaveNoOneBehindEnabled -> Flat
    | LightsGuidanceDamageTakenReduc -> Percent
    | MarkOfDeathChance -> Percent
    | MarkOfDeathDamageRatio -> Percent
    | MarkOfDeathDamageTakenIncrRatio -> Percent
    | NemesisInfusionChance -> Percent
    | PathsEndHealpowerIncr -> Percent
    | PoisonChance -> Percent
    | PoisonCriticalStrikeChanceRatio -> Percent
    | PoisonDamage -> Flat
    | PoisonDamageIncr -> Percent
    | PoisonDamageRatio -> Percent
    | PoisonMaxStacks -> Flat
    | PureInHeartRatio -> Percent
    | ReactiveInstinctDodgeChance -> Percent
    | RingOfInfluenceHealingReceivedIncr -> Percent
    | ShadeRegenerationRatio -> Percent
    | ShieldBashDamageRatio -> Percent
    | ShieldBashEnabled -> Flat
    | SoulBoostPhysicalDamageIncr -> Percent
    | SoulBoostFireDamageIncr -> Percent
    | SurvivalInstinctDodgeChance -> Percent
    | SurvivalInstinctEnabled -> Flat
    | SurvivalInstinctHealingReceived -> Percent
    | SwordAndBoardPaladinSkillLevels -> Flat
    | TauntDamageLess -> Percent
    | TauntDamageTakenMore -> Percent
    | TauntEnabled -> Flat
    | ThunderAssaultDamage -> Flat
    | ThunderAssaultDamageRatio -> Percent
    | ThunderAssaultEnabled -> Flat
    | TimeInNeedHealingRatio -> Percent
    | TyphoonAttackSpeedSlow -> Percent
    | TyphoonEnabled -> Flat
    | TyphoonPhysicalIceDamageTakenIncr -> Percent
    | VampirismBerzerker -> Flat
    | VampirismHealer -> Flat
    | VampirismRanger -> Flat
    | VampirismRatio -> Percent
    | VampirismTank -> Flat
    | VoidChainsHealingReceivedIncr -> Percent
    | VoidInfluenceCastTimeIncr -> Percent
    | VoodooMagicRatio -> Percent
    | WickedBloomRenewChance -> Percent
    | WickedBloomShadowRippleChance -> Percent
    | WorshipShieldRatio -> Percent

    // Sacrifice
    | Sacrifice -> Flat
    | BanishSacrificeAttackSpeedRatioIncr -> Percent
    | CataclysmSacrificeDamageMore -> Percent
    | CleansingRaySacrificeBaseDamageIncr -> Percent
    | DecayAuraSacrificeRatio -> Percent
    | EradicationDamageSacrificeRatio -> Percent
    | NemesisInfusionSacrificeRatio -> Percent
    | SacrificeBerzerkerAttackSpeedIncr -> Percent
    | SacrificeHealpowerIncr -> Percent
    | SacrificeMaximumMana -> Flat
    | SacrificePartyBlockChance -> Percent
    | SacrificePartyDodgeChance -> Percent
    | SacrificePartyMaximumHealthIncr -> Percent
    | SiphonLifeSacrificeDamageIncr -> Percent
    | ShadowRippleSacrificeOverhealRatio -> Percent
    | VampirismSacrificeRatio -> Percent
    | VoidChainsSacrificeHealingReceivedIncr -> Percent
    | VoidZoneShadowSacrificeEntropyDamageRatio -> Percent
    | WickedBloomSacrificeChance -> Percent

module T =
  // Thoth.Json does reflection similar to this each time the auto
  // decoder is used, which is quite slow, at least in fsi. A map lookup
  // is much faster.
  let caseMap =
    Reflection.FSharpType.GetUnionCases typeof<T>
    |> Array.map (fun case -> case.Name, Reflection.FSharpValue.MakeUnion(case,[||]) :?> T)
    |> Map.ofArray

  let decoder : Decoder<T> =
    Decode.string
    |> Decode.andThen (fun str ->
      match caseMap |> Map.tryFind str with
      | Some(attr) -> Decode.succeed attr
      | None -> Decode.fail $"Invalid attribute: {str}"
    )

  // We may want to precompute the reflection here as well if we ever
  // call it from .NET, but in Fable it doesn't seem to be a problem.
  let encoder(attr) =
    let caseInfo, _ = Reflection.FSharpValue.GetUnionFields(attr, typeof<T>)
    Encode.string caseInfo.Name

type StatStick = Map<T, float>

let getAttribute attr map =
  map |> Map.tryFind(attr) |> Option.defaultValue(0.0)

let mergeTwo (stick1: StatStick) (stick2: StatStick): StatStick =
  let folder (acc: StatStick) (attr: T) (value: float) =
    let newValue = value + getAttribute attr acc
    Map.add attr newValue acc
  Map.fold folder stick1 stick2

let merge(sticks: List<StatStick>) =
  sticks |> List.reduce(mergeTwo)

let stack stick num: StatStick =
  stick |> Map.map(fun _attr value -> value * float(num))
